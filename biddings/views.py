# total_item stands for search item or item that are inserted by user
from django.conf import settings
from django.http import response,HttpResponse
from django.http.request import validate_host
from biddings.models import Register,Profile
from django.shortcuts import redirect, render
from django.contrib import messages
from django.contrib.auth import authenticate, login
from datetime import datetime
from .models import Register,Profile,Item


# Create your views here.

def error(request):
   
    return render(request, 'error.html')
    
def index(request):
    
    reg = Register.objects.all()
    return render(request, 'index.html', {'reg': reg})

def search(request):
    if request.method == "GET":
        search = request.GET.get("search")
        search = search.lower()
        new_search = list(search.split(" "))
        search_file = open('search.txt','a')
        search_file.write("\n" + search)
        search_file.close()
        for i in new_search:
            reg = Item.objects.all().filter(total_item = i)
            if reg == " ":
                reg = Item.objects.all().filter(bio = i)
                return render(request, 'search.html')

                
                
        
      
        
    return render(request, 'search.html',{'reg':reg})

def about(request):
    return render(request, 'about.html')


def login(request):
    try:
        if request.method == "POST":
            
            
            name = request.POST.get("name")
            email = request.POST.get("email")
            password = request.POST.get("password")
            
           
            reg = Register.objects.get(name=name,email=email,password=password)
            
            return render(request, 'profile.html',{'reg':reg})
    except Exception:
        messages.error(request, "Invalid Credentials!")
        
        return redirect('/login')

    return render(request, 'login.html')


def register(request):
    try:
        if request.method == "POST":

            name = request.POST.get('name')
            email = request.POST.get('email')
            password = request.POST.get('password')

            reg = Register.objects.filter(email=email).exists()

            if reg != True:

                registers = Register(name=name, email=email,
                                     password=password, date=datetime.today())

                registers.save()
                messages.success(request, "Registered Successfully!")
                return redirect('/login')
            else:

                messages.error(request, "Email already exists!")
                return redirect('/register')
    except Exception as e:

        return redirect('/error')

    return render(request, 'register.html')

def profile(request):
    try:
        if request.method == "POST":
            name = request.POST.get("name")
            email = request.POST.get("email")
            password = request.POST.get("password")
            
            reg = Register.objects.get(name=name,email=email,password=password)
            return render(request, 'profile.html',{'reg':reg})
    except Exception:
        messages.error(request, "Invalid Credentials")
        
        return redirect('/login')
    return render(request, 'profile.html')

def editpro(request):
    try:
        name = request.GET.get("name")
        email = request.GET.get("email")
       
        data = {'name':name,'email':email,}
        #email = request.POST.get("email")
        #password = request.POST.get("password")


        reg = Profile.objects.filter(email=email).exists()

        if reg != True:
            if request.method == "POST":
                #reg = Register.objects.filter(email=email).exists()
                
                address = request.POST.get('address')
                country = request.POST.get('country')
                edit = Profile(name=name, email=email,
                                        address=address, country=country)

                edit.save()
                
                messages.success(request, "Successfully edited your profile ")
                reg = Register.objects.get(name=name,email=email)
                return render(request, 'profile.html',{'reg':reg})
        else:

                messages.error(request, "Cant edit!")
                reg = Register.objects.get(name=name,email=email)
                return render(request, 'profile.html',{'reg':reg})
                
    except Exception:
       return redirect('/error')
        
    return render(request, 'editpro.pug',data) 

def add(request):
    name = request.GET.get("name")
    email = request.GET.get("email")
    reg = Register.objects.get(name=name,email=email)
    try:
        if request.method == "POST":
            
            total_item = request.POST.get('total_item')
            gender = request.POST.get('gender')
            hobby = request.POST.get('hobby')
            income = request.POST.get('income')
            except_amount = request.POST.get('amount')
            age = request.POST.get('age')
            bio = request.POST.get('bio')
            image = request.FILES.get('image')
            adds = Item(name=name, email=email,total_item=total_item,gender=gender,hobby=hobby,
                          income=income,except_amount=except_amount,age=age,bio=bio,image=image)
            adds.save()      
                          
            #print(name,total_item,email,gender,hobby,income,except_amount,age,bio,image)
            return render(request, 'profile.html',{'reg':reg})
    except Exception as ee:
        print(ee)
        messages.error(request,'Urghh!')
        return render(request, 'profile.html',{'reg':reg})
    return render(request, 'add.html',{'reg':reg})