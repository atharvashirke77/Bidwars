from django.contrib import admin
from biddings.models import Item, Register,Profile
# Register your models here.
admin.site.register(Register)
admin.site.register(Profile)
admin.site.register(Item)
